;;; cheatsheet.el - Cheat sheet mode for Emacs

;; Copyright (C) 2013 Evgeniy Sharapov
;; Filename: cheatsheet.el
;; Description: .
;; Author: Evgeniy Sharapov <evgeniy.sharapov@gmail.com>
;; Created: 
;; Version: 0.1
;; Keywords: help
;; X-URL: https://github.com/jwiegley/bind-key
;; Last-Updated: Fri Dec 28 09:52:24 2012 (-0800)
;;           By: dradams
;;     Update #: 1499
;; URL: http://www.emacswiki.org/help-fns+.el
;; Doc URL: http://emacswiki.org/HelpPlus
;; Keywords: help, faces, characters, packages, description
;; Compatibility: GNU Emacs: 22.x, 23.x, 24.x
;;
;; Features that might be required by this library:
;;
;;   `button', `help-fns', `help-mode', `info', `naked', `view',
;;   `wid-edit', `wid-edit+'.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:

