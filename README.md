# Installation

To install this emacs configuration in the command line type while in `HOME` directory:

    git clone https://github.com/evgeniysharapov/DotEmacs.git .emacs.d

After that go into newly created .emacs.d directory

    cd .emacs.d

After that update git modules this Emacs configuration depends on (`use-package`):

    git submodule update --init


# What's Is Configured

# Key Mapping

Few keymaps are created that seem reasonable

#BEGIN RECEIVE ORGTBL keys
| Keyboard Shortcut | Keymap | Purpose |
|---|---|---|
| <kbd>C-x f</kbd> | ctl-x-f-map | File operations |
| <kbd>C-z</kbd> | ctl-z-map | Seconday map |
|  |  |  |
#END RECEIVE ORGTBL keys

<!---
#+ORGTBL: SEND keys orgtbl-to-gfm
| Keyboard Shortcut | Keymap      | Purpose         |
|-------------------+-------------+-----------------|
| <kbd>C-x f</kbd>  | ctl-x-f-map | File operations |
| <kbd>C-z</kbd>    | ctl-z-map   | Seconday map    |
|                   |             |                 |
-->


# TODO #

