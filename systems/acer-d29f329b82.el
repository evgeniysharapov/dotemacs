
;;
;;  Evgeniy Sharapov 
;;  this is a sppecifics config file for mom's laptop in Perm, Russia
;;

;;; set exec-path for mom's laptop
(add-to-list 'exec-path "C:/Program Files/Aspell/bin")

;;; set up git executable for magit on Mom's laptop 
(setq magit-git-executable "C:/Program Files/Git/bin/git.exe")

